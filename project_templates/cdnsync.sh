#!/bin/bash

file="/tmp/.cdnlock"

if [ -e "$file" ]; then
       echo Previous operation still running, exiting...
       exit 0;
else
       if [ ! -e "/var/www/html/$CDN_LOCAL_PATH" ]; then
           echo First run, creating local folder...
           mkdir "/var/www/html/$CDN_LOCAL_PATH"
       fi
       touch $file
       /root/.local/bin/aws s3 sync --acl public-read /var/www/html/$CDN_LOCAL_PATH s3://gulliver-cdn/$CDN_PATH
       /root/.local/bin/aws s3 sync  s3://gulliver-cdn/$CDN_PATH /var/www/html/$CDN_LOCAL_PATH
       rm $file
fi