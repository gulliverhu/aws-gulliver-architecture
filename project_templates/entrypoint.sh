#!/bin/bash

service cron start
chmod +x /var/www/html/logsync.sh
echo -e "59 23 * * * bash -c \"CDN_PATH=$CDN_PATH CDN_LOCAL_PATH=$CDN_LOCAL_PATH AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY /var/www/html/logsync.sh > /tmp/logsync.log 2>&1\""  | crontab

# creating initial directories to avoid php errors...
mkdir /var/www/html/tmp
mkdir /var/www/html/onlinepic/rss
mkdir /var/www/html/onlinepic/statisztika
mkdir /var/www/html/onlinepic/statisztika/statisztika_corporate/
mkdir /var/www/html/log

# access rights to all directories...
chown -R www-data /var/www/html

apache2 -DFOREGROUND