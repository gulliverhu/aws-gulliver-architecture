#!/bin/bash

rm /var/log/apache2/accesslog-*
rm /var/log/apache2/errorlog-*
mv /var/log/apache2/access.log "/var/log/apache2/accesslog-`date '+%Y-%m-%d'`-${HOSTNAME}.log"
mv /var/log/apache2/error.log "/var/log/apache2/errorlog-`date '+%Y-%m-%d'`-${HOSTNAME}.log"
/usr/sbin/apachectl graceful
/root/.local/bin/aws s3 sync --exclude "access.log" --exclude "error.log" --exclude "other_vhosts_access.log" /var/log/apache2 s3://gulliver-logs/$CDN_PATH